import React from 'React'

import {render,screen, waitFor} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import MyNav from "./MyNav";

test('quand je clique', async( => {
  render(<MyNav onResults={} selectedType= {} selectType={'channel'} />);

  await waitFor(() => screen.getAllByRole('menu-item'))
  expect(screen.getByText('Channels')).toHaveClass("active")
}))
