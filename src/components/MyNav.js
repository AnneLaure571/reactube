import Navbar from "react-bootstrap/Navbar";
import { Nav} from "react-bootstrap";
import SearchBar from './SearchBar';

const MyNav = ({onResults, selectedType, selectType}) => {
    return (<Navbar bg="dark" variant="dark">
    <Navbar.Brand href="#home">Navbar</Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link role="menu-item" active={selectedType === "video"} onClick={() => selectType("video")} >Vidéos</Nav.Link>
      <Nav.Link role="menu-item" active={selectedType === "channel"} onClick={() => selectType("channel")}>Channels</Nav.Link>
    </Nav>
      <SearchBar onResults={onResults} selectedType={selectedType}/>
  </Navbar>);
}

export default MyNav;
