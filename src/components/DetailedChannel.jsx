import React from "react";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import  Image from 'react-bootstrap/Image';
import Button from "react-bootstrap/Button";
import {useHistory} from "react-router-dom";

const DetailedChannel = ({id, snippet, statistics, onEscape}) => {
    const {title, channelTitle, description, publishTime, thumbnails} = snippet;
    const {viewCount, commentCount, subscriberCount} = statistics;
    const history = useHistory();
    return (<Card style={{width: '100%'}}>
        <Card.Body>
            <Button variant="danger" style={{float: 'right'}} onClick={history.goBack}>Retour</Button>
            <Row>
            <Col xs={3}>
            <Image src={thumbnails.high.url}fluid={true}rounded/>
            </Col>
                <Col>
                    <h2>{title}</h2>
                    <h5 className="mb-2 text-info">{channelTitle}</h5>
                    <h5 className="mb-2 text-muted">
                        {viewCount} ▶️
                        - {commentCount} 💬
                        - {subscriberCount} abonnés
                    </h5>
                    <Card.Text dangerouslySetInnerHTML={{__html: description.replace(/\n/g, '<br/>')}}/>
                    <Card.Text className="mb-1 text-muted">{publishTime}</Card.Text>
                </Col>
            </Row>
        </Card.Body>
    </Card>);
}

export default DetailedChannel;
