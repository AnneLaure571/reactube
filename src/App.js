import React, {useState} from 'react';
import {Container} from 'react-bootstrap';
import MyNav from './components/MyNav';

import Video from "./components/Video";
import DetailedVideo from "./components/DetailedVideo";

import Channel from "./components/Channel";
import DetailedChannel from "./components/DetailedChannel";

import './App.css';

//router
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

const App = () => {
  const [videos, setVideos] = useState([]);
  const [selectedVideo, selectVideo] = useState(null);
  const [selectedChannel, selectChannel] = useState(null);
  const [selectedType, selectType] = useState("video");
  return (<Container className="p-3">
                <Router>
                   <MyNav onResults={setVideos} selectedType={selectedType} selectType={selectType}/>
                      <Switch>
                        <Route path="/videos/:videoId">
                           {selectedVideo != null &&
                           <DetailedVideo id={selectedVideo.id}
                                          snippet={selectedVideo.snippet} player={selectedVideo.player}
                                          statistics={selectedVideo.statistics}
                                          onEscape={() => selectVideo(null)}/>}
                        </Route>
                        <Route path="/search/video/:search">
                           <>
                               {videos.map(v => {
                                   const {
                                       title, description, thumbnails,
                                       channelTitle, publishTime
                                   } = v.snippet;

                                   return (<Video
                                       videoId={v.id.videoId}
                                       thumbnail={thumbnails.high}
                                       description={description}
                                       channelTitle={channelTitle}
                                       publishTime={publishTime}
                                       title={title}
                                       selectVideo={selectVideo}/>);
                               })}
                           </>
                       </Route>
                       <Route path="/channels/:channelId">
                       {selectedChannel != null &&
                       <DetailedChannel id={selectedChannel.id}
                                      snippet={selectedChannel.snippet}
                                      statistics={selectedChannel.statistics}
                                      onEscape={() => selectChannel(null)}/>}
                      </Route>
                      <Route path="/search/channel/:search">
                         <>
                             {videos.map(v => {
                                 const {
                                     title, description, thumbnails,
                                     channelTitle, publishTime
                                 } = v.snippet;

                                 return (<Channel
                                     channelId={v.id.channelId}
                                     thumbnail={thumbnails.high}
                                     description={description}
                                     channelTitle={channelTitle}
                                     publishTime={publishTime}
                                     title={title}
                                     selectChannel={selectChannel}/>);
                             })}
                         </>
                     </Route>
                       <Route path="/">
                           Merci d'effectuer une recherche...
                       </Route>
                   </Switch>
               </Router>
          </Container>);
    };
export default App;
